# Service Usage Plots

In this project, plots are generated from service usage data.
The data is incorporated via submodules from separate projects.


##  Generated Plots (most recent)

All plots are generated automatically when changes are detected or the generation pipeline is triggered.
Also daily plots are generated.

### General overview plots

* [**Latest build, all graphs**](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/browse/artifact/plots?job=plot_general)
* [Download all graphs in zip file](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/download/artifact/plots?job=plot_general)
* [Gallery Overview](gallery/gallery.md)
* [Graphs pdf collection](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/plots.pdf?job=plot_general)
* Example of inline svg (AAI users):  
  ![AAI users](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/helmholtz-aai/user_per_group.svg?job=plot_general)

### KPI plots
* [**Latest build, all graphs**](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/browse/artifact/kpi_plots?job=plot_general)
* [Download all graphs in zip file](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/download/artifact/kpi_plots?job=plot_general)
* [KPI Gallery Overview](gallery/gallery.md#overview-of-kpi-plots)
* [Graphs pdf collection](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/kpi_plots.pdf?job=plot_general)
    * see [here for documentation of the Helmholtz Cloud Service Operation KPI](https://hifis.net/doc/cloud-service-kpi/).
* Contributions of services to overall KPI:  
  ![Bar plot of contributions of services to overall KPI](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_contributions_per_service.svg?job=plot_general)
* Example of KPI plot (HZDR Codebase):  
  ![HZDR codebase users](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/hzdr-codebase/last_active_7_days.svg?job=plot_general)

## Local execution

Clone this repository *recursively, i.e. including the data*, with:
```
git clone --recursive git@codebase.helmholtz.cloud:hifis/overall/kpi/kpi-plots-ci.git
```

If you have cloned the repository already, you might need to update the submodules (i.e., data) with
```
git submodule sync --recursive
git submodule update --remote --recursive --init
```

To run the plot script, you need python and the following libraries installed:
- matplotlib
- pypdf
- pandas
- mattermost

To run the script:
```
python plot_scripts/plot.py
```

## Structure
* `subprojects`: Linked projects providing the source data.
* `gallery`: The gallery is automatically generated in this folder
* `metadata`: This directory contains metadata on the individual services. 
    * `displayname`: The name to be displayed for this service
    * `T_onboard`: The onboarding date of the service
    * `T_reference_overall`: A specially defined date from which the dates of the service are used
    * `special_plot_%`: Defines whether a specific data file is meant as a special plot
    * `grace_period`: period before warnings are sent if no new data appears
* `kpi-weights.csv`: Weighting of all separate service contributions to overall KPI; usually 50% service users+50% service usage.
* `kpi_history.csv`: The current KPI value is entered here on each day

## Wiki
[Link to wiki](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/wikis/home)
