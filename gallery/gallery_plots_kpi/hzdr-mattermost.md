# Mattermost (HZDR)

##### post_count.svg
![post_count.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/hzdr-mattermost/mattermost_statistics-post_count.svg?job=plot_general)

##### team_count.svg
![team_count.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/hzdr-mattermost/mattermost_statistics-team_count.svg?job=plot_general)

##### daily_active_users.svg
![daily_active_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/hzdr-mattermost/mattermost_statistics-daily_active_users.svg?job=plot_general)

##### monthly_active_users.svg
![monthly_active_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/hzdr-mattermost/mattermost_statistics-monthly_active_users.svg?job=plot_general)

