# Helmholtz KG (FZJ)

##### Search_Index_Page_Requests.svg
![Search_Index_Page_Requests.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/fzj-helmholtz-kg/statistics-Search_Index_Page_Requests.svg?job=plot_general)

##### Search_Requests.svg
![Search_Requests.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/fzj-helmholtz-kg/statistics-Search_Requests.svg?job=plot_general)

##### Sparql_Index_Page_Requests.svg
![Sparql_Index_Page_Requests.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/fzj-helmholtz-kg/statistics-Sparql_Index_Page_Requests.svg?job=plot_general)

##### Sparql_Requests.svg
![Sparql_Requests.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/fzj-helmholtz-kg/statistics-Sparql_Requests.svg?job=plot_general)

