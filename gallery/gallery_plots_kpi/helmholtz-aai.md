# Helmholtz AAI

##### Connected_Helmholtz_Centres.svg
![Connected_Helmholtz_Centres.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/helmholtz-aai/aai_centers-Connected_Helmholtz_Centres.svg?job=plot_general)

##### cumulated_services.svg
![cumulated_services.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/helmholtz-aai/aai_services-cumulated_services.svg?job=plot_general)

##### all_IdP.svg
![all_IdP.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/helmholtz-aai/aai_userstats-all_IdP.svg?job=plot_general)

##### Active_User.svg
![Active_User.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/helmholtz-aai/active_user-Active_User.svg?job=plot_general)

