# GPU Compute (HZDR)

##### storage_total.svg
![storage_total.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/hzdr-gpu-compute/hifis_gpu_stats-storage_total.svg?job=plot_general)

##### users_total.svg
![users_total.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/hzdr-gpu-compute/hifis_gpu_stats-users_total.svg?job=plot_general)

##### users_active_12h.svg
![users_active_12h.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/hzdr-gpu-compute/hifis_gpu_stats-users_active_12h.svg?job=plot_general)

