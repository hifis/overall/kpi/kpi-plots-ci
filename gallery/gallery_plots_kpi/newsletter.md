# newsletter

##### newsletter_subscribers.svg
![newsletter_subscribers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/newsletter/data-newsletter_subscribers.svg?job=plot_general)

##### announcement_subscribers.svg
![announcement_subscribers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/newsletter/data-announcement_subscribers.svg?job=plot_general)

