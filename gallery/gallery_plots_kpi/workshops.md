# workshops

##### participant_hours_per_month.svg
![participant_hours_per_month.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/workshops/data-participant_hours_per_month.svg?job=plot_general)

##### cumulated_participant_hours.svg
![cumulated_participant_hours.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/workshops/data-cumulated_participant_hours.svg?job=plot_general)

##### cumulated_participant_hours_per_year.svg
![cumulated_participant_hours_per_year.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/workshops/data-cumulated_participant_hours_per_year.svg?job=plot_general)

