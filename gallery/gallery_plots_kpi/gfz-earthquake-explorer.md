# gfz-earthquake-explorer

##### unique_visitors.svg
![unique_visitors.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/gfz-earthquake-explorer/data-unique_visitors.svg?job=plot_general)

##### visits.svg
![visits.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/gfz-earthquake-explorer/data-visits.svg?job=plot_general)

##### pageviews.svg
![pageviews.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/gfz-earthquake-explorer/data-pageviews.svg?job=plot_general)

##### avg_page_load_time.svg
![avg_page_load_time.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/gfz-earthquake-explorer/data-avg_page_load_time.svg?job=plot_general)

##### stored_events.svg
![stored_events.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/gfz-earthquake-explorer/data-stored_events.svg?job=plot_general)

##### stored_mt.svg
![stored_mt.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/gfz-earthquake-explorer/data-stored_mt.svg?job=plot_general)

##### stored_extra.svg
![stored_extra.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/gfz-earthquake-explorer/data-stored_extra.svg?job=plot_general)

