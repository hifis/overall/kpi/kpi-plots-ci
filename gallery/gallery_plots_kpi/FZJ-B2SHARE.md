# B2Share (FZJ)

##### Disk_Space.svg
![Disk_Space.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/FZJ-B2SHARE/b2share_accounting-Disk_Space.svg?job=plot_general)

##### Records.svg
![Records.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/FZJ-B2SHARE/b2share_accounting-Records.svg?job=plot_general)

##### Files.svg
![Files.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/FZJ-B2SHARE/b2share_accounting-Files.svg?job=plot_general)

