# bwSyncnShare (KIT)

##### active_bwsyncandsahre_users_last_24_h.svg
![active_bwsyncandsahre_users_last_24_h.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/KIT-bwsns/data-active_bwsyncandsahre_users_last_24_h.svg?job=plot_general)

##### all_bwsyncandshare_users.svg
![all_bwsyncandshare_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/KIT-bwsns/data-all_bwsyncandshare_users.svg?job=plot_general)

##### all_bwsyncandshare_files.svg
![all_bwsyncandshare_files.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/KIT-bwsns/data-all_bwsyncandshare_files.svg?job=plot_general)

##### all_bwsyncandshare_shares.svg
![all_bwsyncandshare_shares.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/KIT-bwsns/data-all_bwsyncandshare_shares.svg?job=plot_general)

##### HIFIS_Users_Percentage.svg
![HIFIS_Users_Percentage.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/KIT-bwsns/data-HIFIS_Users_Percentage.svg?job=plot_general)

##### helmholtz_bwsyncandshare_users.svg
![helmholtz_bwsyncandshare_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/KIT-bwsns/data-helmholtz_bwsyncandshare_users.svg?job=plot_general)

