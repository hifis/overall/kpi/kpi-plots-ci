# SciFlow (HZB)

##### minutes_all.svg
![minutes_all.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/hzb-sciflow/HZB-Sciflow-KPI-minutes_all.svg?job=plot_general)

##### users_all.svg
![users_all.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/hzb-sciflow/HZB-Sciflow-KPI-users_all.svg?job=plot_general)

##### new_users_all.svg
![new_users_all.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/hzb-sciflow/HZB-Sciflow-KPI-new_users_all.svg?job=plot_general)

##### users_total_all.svg
![users_total_all.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/hzb-sciflow/HZB-Sciflow-KPI-users_total_all.svg?job=plot_general)

##### documents_new_all.svg
![documents_new_all.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/hzb-sciflow/HZB-Sciflow-KPI-documents_new_all.svg?job=plot_general)

##### documents_total_all.svg
![documents_total_all.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/hzb-sciflow/HZB-Sciflow-KPI-documents_total_all.svg?job=plot_general)

