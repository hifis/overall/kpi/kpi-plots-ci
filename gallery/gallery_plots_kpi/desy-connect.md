# desy-connect

##### Experts.svg
![Experts.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/desy-connect/data-Experts.svg?job=plot_general)

##### Entries.svg
![Entries.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/desy-connect/data-Entries.svg?job=plot_general)

##### Comment.svg
![Comment.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/kpi_plots/desy-connect/data-Comment.svg?job=plot_general)

