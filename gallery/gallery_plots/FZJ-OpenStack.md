# OpenStack (FZJ)

##### CPU_Hours.svg
![CPU_Hours.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-OpenStack/usage-stats-openstack-jsc-weekly-CPU_Hours.svg?job=plot_general)

##### RAM_MB-Hours.svg
![RAM_MB-Hours.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-OpenStack/usage-stats-openstack-jsc-weekly-RAM_MB-Hours.svg?job=plot_general)

##### Disk_GB-Hours.svg
![Disk_GB-Hours.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-OpenStack/usage-stats-openstack-jsc-weekly-Disk_GB-Hours.svg?job=plot_general)

##### Servers.svg
![Servers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-OpenStack/usage-stats-openstack-jsc-weekly-Servers.svg?job=plot_general)

##### Projects.svg
![Projects.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-OpenStack/usage-stats-openstack-jsc-weekly-Projects.svg?job=plot_general)

