# Rancher (DESY)

##### Users.svg
![Users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-rancher/desy-rancher-user-kpi-Users.svg?job=plot_general)

##### Projects.svg
![Projects.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-rancher/desy-rancher-user-kpi-Projects.svg?job=plot_general)

##### Number_of_volumes.svg
![Number_of_volumes.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-rancher/desy-rancher-user-kpi-Number_of_volumes.svg?job=plot_general)

##### Size_of_volumes_[GiB].svg
![Size_of_volumes_[GiB].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-rancher/desy-rancher-user-kpi-Size_of_volumes_[GiB].svg?job=plot_general)

##### Number_of_pods.svg
![Number_of_pods.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-rancher/desy-rancher-user-kpi-Number_of_pods.svg?job=plot_general)

##### Number_of_ingress.svg
![Number_of_ingress.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-rancher/desy-rancher-user-kpi-Number_of_ingress.svg?job=plot_general)

##### CPU_usage_from_pods_[h].svg
![CPU_usage_from_pods_[h].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-rancher/desy-rancher-user-kpi-CPU_usage_from_pods_[h].svg?job=plot_general)

##### Memory_usage_from_pods_[GB].svg
![Memory_usage_from_pods_[GB].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-rancher/desy-rancher-user-kpi-Memory_usage_from_pods_[GB].svg?job=plot_general)

