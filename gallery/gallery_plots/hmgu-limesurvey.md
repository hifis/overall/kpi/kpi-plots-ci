# LimeSurvey (HMGU)

##### total_surveys.svg
![total_surveys.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hmgu-limesurvey/usage-stats-hmgu-limesurvey-weekly-total_surveys.svg?job=plot_general)

##### active_surveys.svg
![active_surveys.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hmgu-limesurvey/usage-stats-hmgu-limesurvey-weekly-active_surveys.svg?job=plot_general)

##### users.svg
![users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hmgu-limesurvey/usage-stats-hmgu-limesurvey-weekly-users.svg?job=plot_general)

##### survey_admins.svg
![survey_admins.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hmgu-limesurvey/usage-stats-hmgu-limesurvey-weekly-survey_admins.svg?job=plot_general)

##### survey_admin_groups.svg
![survey_admin_groups.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hmgu-limesurvey/usage-stats-hmgu-limesurvey-weekly-survey_admin_groups.svg?job=plot_general)

##### unique_institutes.svg
![unique_institutes.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hmgu-limesurvey/usage-stats-hmgu-limesurvey-weekly-unique_institutes.svg?job=plot_general)

##### total_questions_of_active_surveys.svg
![total_questions_of_active_surveys.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hmgu-limesurvey/usage-stats-hmgu-limesurvey-weekly-total_questions_of_active_surveys.svg?job=plot_general)

##### new_surveys_within_the_week.svg
![new_surveys_within_the_week.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hmgu-limesurvey/usage-stats-hmgu-limesurvey-weekly-new_surveys_within_the_week.svg?job=plot_general)

