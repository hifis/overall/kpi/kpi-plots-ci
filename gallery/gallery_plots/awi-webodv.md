# webODV (AWI)

##### Number_of_daily_users.svg
![Number_of_daily_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/awi-webodv/data-Number_of_daily_users.svg?job=plot_general)

##### Number_of_daily_users_(intern_AWI).svg
![Number_of_daily_users_(intern_AWI).svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/awi-webodv/data-Number_of_daily_users_(intern_AWI).svg?job=plot_general)

##### Number_of_daily_users_(extern).svg
![Number_of_daily_users_(extern).svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/awi-webodv/data-Number_of_daily_users_(extern).svg?job=plot_general)

##### Total_number_of_daily_ODV_instances.svg
![Total_number_of_daily_ODV_instances.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/awi-webodv/data-Total_number_of_daily_ODV_instances.svg?job=plot_general)

##### Number_of_daily_data_imports.svg
![Number_of_daily_data_imports.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/awi-webodv/data-Number_of_daily_data_imports.svg?job=plot_general)

##### Number_of_daily_Explorers.svg
![Number_of_daily_Explorers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/awi-webodv/data-Number_of_daily_Explorers.svg?job=plot_general)

##### Number_of_daily_Extractors.svg
![Number_of_daily_Extractors.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/awi-webodv/data-Number_of_daily_Extractors.svg?job=plot_general)

##### Total_used_storage.svg
![Total_used_storage.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/awi-webodv/data-Total_used_storage.svg?job=plot_general)

