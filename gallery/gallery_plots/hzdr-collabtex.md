# Collabtex (HZDR)

##### number_of_users.svg
![number_of_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-collabtex/active_users-number_of_users.svg?job=plot_general)

##### last_active_1_days.svg
![last_active_1_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-collabtex/active_users-last_active_1_days.svg?job=plot_general)

##### last_active_7_days.svg
![last_active_7_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-collabtex/active_users-last_active_7_days.svg?job=plot_general)

##### last_active_14_days.svg
![last_active_14_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-collabtex/active_users-last_active_14_days.svg?job=plot_general)

##### last_active_30_days.svg
![last_active_30_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-collabtex/active_users-last_active_30_days.svg?job=plot_general)

##### last_active_60_days.svg
![last_active_60_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-collabtex/active_users-last_active_60_days.svg?job=plot_general)

##### overleaf_email_domains.svg
![overleaf_email_domains.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-collabtex/overleaf_email_domains.svg?job=plot_general)

##### overleaf_user_origins.svg
![overleaf_user_origins.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-collabtex/overleaf_user_origins.svg?job=plot_general)

##### number_of_projects.svg
![number_of_projects.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-collabtex/project_statistics-number_of_projects.svg?job=plot_general)

##### last_active_1_days.svg
![last_active_1_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-collabtex/project_statistics-last_active_1_days.svg?job=plot_general)

##### last_active_7_days.svg
![last_active_7_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-collabtex/project_statistics-last_active_7_days.svg?job=plot_general)

##### last_active_14_days.svg
![last_active_14_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-collabtex/project_statistics-last_active_14_days.svg?job=plot_general)

##### last_active_30_days.svg
![last_active_30_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-collabtex/project_statistics-last_active_30_days.svg?job=plot_general)

##### last_active_60_days.svg
![last_active_60_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-collabtex/project_statistics-last_active_60_days.svg?job=plot_general)

