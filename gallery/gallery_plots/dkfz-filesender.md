# FileSender (DKFZ)

##### _user_count.svg
![_user_count.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-filesender/usage-stats-dkfz-filesender-weekly-_user_count.svg?job=plot_general)

##### _active_external_users.svg
![_active_external_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-filesender/usage-stats-dkfz-filesender-weekly-_active_external_users.svg?job=plot_general)

##### _available_transfers.svg
![_available_transfers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-filesender/usage-stats-dkfz-filesender-weekly-_available_transfers.svg?job=plot_general)

##### _created_transfers.svg
![_created_transfers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-filesender/usage-stats-dkfz-filesender-weekly-_created_transfers.svg?job=plot_general)

##### _created_transfers_by_active_external_users.svg
![_created_transfers_by_active_external_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-filesender/usage-stats-dkfz-filesender-weekly-_created_transfers_by_active_external_users.svg?job=plot_general)

##### _files_uploaded.svg
![_files_uploaded.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-filesender/usage-stats-dkfz-filesender-weekly-_files_uploaded.svg?job=plot_general)

##### _files_downloaded.svg
![_files_downloaded.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-filesender/usage-stats-dkfz-filesender-weekly-_files_downloaded.svg?job=plot_general)

##### _transfered_up.svg
![_transfered_up.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-filesender/usage-stats-dkfz-filesender-weekly-_transfered_up.svg?job=plot_general)

##### _transfered_down.svg
![_transfered_down.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-filesender/usage-stats-dkfz-filesender-weekly-_transfered_down.svg?job=plot_general)

##### _used_space.svg
![_used_space.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-filesender/usage-stats-dkfz-filesender-weekly-_used_space.svg?job=plot_general)

