# Codebase (HZDR)

##### number_of_users.svg
![number_of_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/active_users-number_of_users.svg?job=plot_general)

##### last_active_1_days.svg
![last_active_1_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/active_users-last_active_1_days.svg?job=plot_general)

##### last_active_7_days.svg
![last_active_7_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/active_users-last_active_7_days.svg?job=plot_general)

##### last_active_14_days.svg
![last_active_14_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/active_users-last_active_14_days.svg?job=plot_general)

##### last_active_30_days.svg
![last_active_30_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/active_users-last_active_30_days.svg?job=plot_general)

##### last_active_60_days.svg
![last_active_60_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/active_users-last_active_60_days.svg?job=plot_general)

##### gitlab_mail_domains.svg
![gitlab_mail_domains.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/gitlab_mail_domains.svg?job=plot_general)

##### number_of_projects.svg
![number_of_projects.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/project_statistics-number_of_projects.svg?job=plot_general)

##### total_commit_count.svg
![total_commit_count.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/project_statistics-total_commit_count.svg?job=plot_general)

##### total_storage_size.svg
![total_storage_size.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/project_statistics-total_storage_size.svg?job=plot_general)

##### total_repository_size.svg
![total_repository_size.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/project_statistics-total_repository_size.svg?job=plot_general)

##### total_wiki_size.svg
![total_wiki_size.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/project_statistics-total_wiki_size.svg?job=plot_general)

##### total_lfs_object_size.svg
![total_lfs_object_size.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/project_statistics-total_lfs_object_size.svg?job=plot_general)

##### total_job_artifacts_size.svg
![total_job_artifacts_size.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/project_statistics-total_job_artifacts_size.svg?job=plot_general)

##### last_active_1_days.svg
![last_active_1_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/project_statistics-last_active_1_days.svg?job=plot_general)

##### last_active_7_days.svg
![last_active_7_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/project_statistics-last_active_7_days.svg?job=plot_general)

##### last_active_14_days.svg
![last_active_14_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/project_statistics-last_active_14_days.svg?job=plot_general)

##### last_active_30_days.svg
![last_active_30_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/project_statistics-last_active_30_days.svg?job=plot_general)

##### last_active_60_days.svg
![last_active_60_days.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/hzdr-codebase/project_statistics-last_active_60_days.svg?job=plot_general)

