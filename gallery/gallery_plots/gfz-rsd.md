# RSD (GFZ)

##### Accounts_at_GFZ.svg
![Accounts_at_GFZ.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-rsd/data-Accounts_at_GFZ.svg?job=plot_general)

##### Helmholtz_accounts.svg
![Helmholtz_accounts.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-rsd/data-Helmholtz_accounts.svg?job=plot_general)

##### Accounts_in_total.svg
![Accounts_in_total.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-rsd/data-Accounts_in_total.svg?job=plot_general)

##### New_accounts.svg
![New_accounts.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-rsd/data-New_accounts.svg?job=plot_general)

##### Deprovisioned_accounts.svg
![Deprovisioned_accounts.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-rsd/data-Deprovisioned_accounts.svg?job=plot_general)

##### Software_entries.svg
![Software_entries.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-rsd/data-Software_entries.svg?job=plot_general)

##### Projects.svg
![Projects.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-rsd/data-Projects.svg?job=plot_general)

##### Organisations_in_total.svg
![Organisations_in_total.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-rsd/data-Organisations_in_total.svg?job=plot_general)

##### Software_mentions.svg
![Software_mentions.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-rsd/data-Software_mentions.svg?job=plot_general)

##### Contributor_count.svg
![Contributor_count.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-rsd/data-Contributor_count.svg?job=plot_general)

