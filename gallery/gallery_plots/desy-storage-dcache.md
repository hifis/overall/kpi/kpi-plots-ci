# InfiniteSpace (DESY)

##### usage_via_Helmholtz_AAI_[TiB].svg
![usage_via_Helmholtz_AAI_[TiB].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-storage-dcache/data-usage_via_Helmholtz_AAI_[TiB].svg?job=plot_general)

##### non_Helmholtz_namespace_[TiB].svg
![non_Helmholtz_namespace_[TiB].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-storage-dcache/data-non_Helmholtz_namespace_[TiB].svg?job=plot_general)

##### OpenData_[TiB].svg
![OpenData_[TiB].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-storage-dcache/data-OpenData_[TiB].svg?job=plot_general)

##### total_usage_[TiB].svg
![total_usage_[TiB].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-storage-dcache/data-total_usage_[TiB].svg?job=plot_general)

##### number_of_distinct_VOs_with_data.svg
![number_of_distinct_VOs_with_data.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-storage-dcache/data-number_of_distinct_VOs_with_data.svg?job=plot_general)

##### hifis_namespace_total_[MB].svg
![hifis_namespace_total_[MB].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-storage-dcache/data-hifis_namespace_total_[MB].svg?job=plot_general)

##### hifis_namespace_free_[MB].svg
![hifis_namespace_free_[MB].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-storage-dcache/data-hifis_namespace_free_[MB].svg?job=plot_general)

##### hifis_namespace_used_[TiB].svg
![hifis_namespace_used_[TiB].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-storage-dcache/data-hifis_namespace_used_[TiB].svg?job=plot_general)

##### Quota_(TiB].svg
![Quota_(TiB].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-storage-dcache/data-Quota_(TiB].svg?job=plot_general)

##### Quota_used_[TiB].svg
![Quota_used_[TiB].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-storage-dcache/data-Quota_used_[TiB].svg?job=plot_general)

