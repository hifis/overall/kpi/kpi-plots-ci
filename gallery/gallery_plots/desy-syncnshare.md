# SyncnShare (DESY)

##### Folders_(external).svg
![Folders_(external).svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-syncnshare/consumption-Folders_(external).svg?job=plot_general)

##### Files_(external).svg
![Files_(external).svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-syncnshare/consumption-Files_(external).svg?job=plot_general)

##### Storage_[kB]_(external).svg
![Storage_[kB]_(external).svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-syncnshare/consumption-Storage_[kB]_(external).svg?job=plot_general)

##### Folders_(all).svg
![Folders_(all).svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-syncnshare/consumption-Folders_(all).svg?job=plot_general)

##### Files_(all).svg
![Files_(all).svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-syncnshare/consumption-Files_(all).svg?job=plot_general)

##### Storage_[kB]_(all).svg
![Storage_[kB]_(all).svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-syncnshare/consumption-Storage_[kB]_(all).svg?job=plot_general)

##### Users_authenticated_via_Keycloak\Helmholtz_AAI.svg
![Users_authenticated_via_Keycloak\Helmholtz_AAI.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-syncnshare/users_aai-Users_authenticated_via_Keycloak\Helmholtz_AAI.svg?job=plot_general)

##### Registered_users.svg
![Registered_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-syncnshare/users_all-Registered_users.svg?job=plot_general)

