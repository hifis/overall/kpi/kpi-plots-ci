# HIFIS Events (DESY)

##### files.svg
![files.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-indico-events/data-files.svg?job=plot_general)

##### total_contributions.svg
![total_contributions.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-indico-events/data-total_contributions.svg?job=plot_general)

##### total_events.svg
![total_events.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-indico-events/data-total_events.svg?job=plot_general)

##### users.svg
![users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-indico-events/data-users.svg?job=plot_general)

##### deleted_users.svg
![deleted_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-indico-events/data-deleted_users.svg?job=plot_general)

