# Juypter (FZJ)

##### Accounts_total.svg
![Accounts_total.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-Jupyter/data-Accounts_total.svg?job=plot_general)

##### Accounts_Helmholtz.svg
![Accounts_Helmholtz.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-Jupyter/data-Accounts_Helmholtz.svg?job=plot_general)

##### Accounts_new.svg
![Accounts_new.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-Jupyter/data-Accounts_new.svg?job=plot_general)

##### Deprovisioned_accounts.svg
![Deprovisioned_accounts.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-Jupyter/data-Deprovisioned_accounts.svg?job=plot_general)

##### Domains_total.svg
![Domains_total.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-Jupyter/data-Domains_total.svg?job=plot_general)

##### Organisations_Helmholtz.svg
![Organisations_Helmholtz.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-Jupyter/data-Organisations_Helmholtz.svg?job=plot_general)

##### successful_jobs.svg
![successful_jobs.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-Jupyter/usage-stats-jupyter-jsc-weekly-successful_jobs.svg?job=plot_general)

##### active_users.svg
![active_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-Jupyter/usage-stats-jupyter-jsc-weekly-active_users.svg?job=plot_general)

##### nodes.svg
![nodes.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-Jupyter/usage-stats-jupyter-jsc-weekly-nodes.svg?job=plot_general)

##### gpus.svg
![gpus.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-Jupyter/usage-stats-jupyter-jsc-weekly-gpus.svg?job=plot_general)

##### 85.svg
![85.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-Jupyter/user_metrics_last_1_days-85.svg?job=plot_general)

##### 313.svg
![313.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-Jupyter/user_metrics_last_30_days-313.svg?job=plot_general)

##### 88.svg
![88.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/FZJ-Jupyter/user_metrics_last_7_days-88.svg?job=plot_general)

