# Jupyter (DESY)

##### Total_number_of_Users_[Users].svg
![Total_number_of_Users_[Users].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-jupyter/data-Total_number_of_Users_[Users].svg?job=plot_general)

##### active_users.svg
![active_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-jupyter/data-active_users.svg?job=plot_general)

##### cpu_h.svg
![cpu_h.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-jupyter/data-cpu_h.svg?job=plot_general)

