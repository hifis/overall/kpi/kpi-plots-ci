# Sensor Management System (GFZ)

##### configuration_pids.svg
![configuration_pids.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-sensor-mgmt/data-configuration_pids.svg?job=plot_general)

##### configurations.svg
![configurations.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-sensor-mgmt/data-configurations.svg?job=plot_general)

##### datastreams.svg
![datastreams.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-sensor-mgmt/data-datastreams.svg?job=plot_general)

##### device_pids.svg
![device_pids.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-sensor-mgmt/data-device_pids.svg?job=plot_general)

##### devices.svg
![devices.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-sensor-mgmt/data-devices.svg?job=plot_general)

##### orcids.svg
![orcids.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-sensor-mgmt/data-orcids.svg?job=plot_general)

##### organizations.svg
![organizations.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-sensor-mgmt/data-organizations.svg?job=plot_general)

##### pids.svg
![pids.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-sensor-mgmt/data-pids.svg?job=plot_general)

##### platform_pids.svg
![platform_pids.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-sensor-mgmt/data-platform_pids.svg?job=plot_general)

##### platforms.svg
![platforms.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-sensor-mgmt/data-platforms.svg?job=plot_general)

##### site_pids.svg
![site_pids.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-sensor-mgmt/data-site_pids.svg?job=plot_general)

##### sites.svg
![sites.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-sensor-mgmt/data-sites.svg?job=plot_general)

##### uploads.svg
![uploads.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-sensor-mgmt/data-uploads.svg?job=plot_general)

##### users.svg
![users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-sensor-mgmt/data-users.svg?job=plot_general)

