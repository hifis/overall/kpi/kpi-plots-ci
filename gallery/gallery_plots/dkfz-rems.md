# REMS (DKFZ)

##### _enabled_organizations.svg
![_enabled_organizations.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-rems/usage-stats-dkfz-rems-weekly-_enabled_organizations.svg?job=plot_general)

##### _total_users.svg
![_total_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-rems/usage-stats-dkfz-rems-weekly-_total_users.svg?job=plot_general)

##### _external_users.svg
![_external_users.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-rems/usage-stats-dkfz-rems-weekly-_external_users.svg?job=plot_general)

##### _enabled_catalogue_items.svg
![_enabled_catalogue_items.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-rems/usage-stats-dkfz-rems-weekly-_enabled_catalogue_items.svg?job=plot_general)

##### _submitted_applications.svg
![_submitted_applications.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-rems/usage-stats-dkfz-rems-weekly-_submitted_applications.svg?job=plot_general)

##### _approved_applications.svg
![_approved_applications.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-rems/usage-stats-dkfz-rems-weekly-_approved_applications.svg?job=plot_general)

##### _rejected_applications.svg
![_rejected_applications.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/dkfz-rems/usage-stats-dkfz-rems-weekly-_rejected_applications.svg?job=plot_general)

