# Notes (DESY)

##### peak_onlineNotes.svg
![peak_onlineNotes.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-daily-peak_onlineNotes.svg?job=plot_general)

##### peak_onlineUsers.svg
![peak_onlineUsers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-daily-peak_onlineUsers.svg?job=plot_general)

##### peak_notesCount.svg
![peak_notesCount.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-daily-peak_notesCount.svg?job=plot_general)

##### peak_registeredUsers.svg
![peak_registeredUsers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-daily-peak_registeredUsers.svg?job=plot_general)

##### peak_onlineRegisteredUsers.svg
![peak_onlineRegisteredUsers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-daily-peak_onlineRegisteredUsers.svg?job=plot_general)

##### peak_onlineNotes.svg
![peak_onlineNotes.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-weekly-peak_onlineNotes.svg?job=plot_general)

##### peak_onlineUsers.svg
![peak_onlineUsers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-weekly-peak_onlineUsers.svg?job=plot_general)

##### peak_notesCount.svg
![peak_notesCount.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-weekly-peak_notesCount.svg?job=plot_general)

##### peak_registeredUsers.svg
![peak_registeredUsers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-weekly-peak_registeredUsers.svg?job=plot_general)

##### peak_onlineRegisteredUsers.svg
![peak_onlineRegisteredUsers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-weekly-peak_onlineRegisteredUsers.svg?job=plot_general)

##### onlineNotes.svg
![onlineNotes.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-onlineNotes.svg?job=plot_general)

##### onlineUsers.svg
![onlineUsers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-onlineUsers.svg?job=plot_general)

##### notesCount.svg
![notesCount.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-notesCount.svg?job=plot_general)

##### registeredUsers.svg
![registeredUsers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-registeredUsers.svg?job=plot_general)

##### onlineRegisteredUsers.svg
![onlineRegisteredUsers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/desy-notes/data-onlineRegisteredUsers.svg?job=plot_general)

