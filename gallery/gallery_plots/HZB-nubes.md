# nubes (HZB)

##### activeLast24hours.svg
![activeLast24hours.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/kpi-new-activeLast24hours.svg?job=plot_general)

##### numUsers.svg
![numUsers.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/kpi-new-numUsers.svg?job=plot_general)

##### numFiles.svg
![numFiles.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/kpi-new-numFiles.svg?job=plot_general)

##### numShares.svg
![numShares.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/kpi-new-numShares.svg?job=plot_general)

##### total_sizeGroupfolders.svg
![total_sizeGroupfolders.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/kpi-new-total_sizeGroupfolders.svg?job=plot_general)

##### numGroupfoldersHZB.svg
![numGroupfoldersHZB.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/kpi-new-numGroupfoldersHZB.svg?job=plot_general)

##### numGroupfoldersHIFIS.svg
![numGroupfoldersHIFIS.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/kpi-new-numGroupfoldersHIFIS.svg?job=plot_general)

##### numGroupfolders.svg
![numGroupfolders.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/kpi-new-numGroupfolders.svg?job=plot_general)

##### active_user.svg
![active_user.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/KPi_nubes-active_user.svg?job=plot_general)

##### folders.svg
![folders.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/KPi_nubes-folders.svg?job=plot_general)

##### files.svg
![files.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/KPi_nubes-files.svg?job=plot_general)

##### user-storage_[G].svg
![user-storage_[G].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/KPi_nubes-user-storage_[G].svg?job=plot_general)

##### groupfolders_–_on_top.svg
![groupfolders_–_on_top.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/KPi_nubes-groupfolders_–_on_top.svg?job=plot_general)

##### groupfolders.svg
![groupfolders.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/KPi_nubes-groupfolders.svg?job=plot_general)

##### groupfolder-files.svg
![groupfolder-files.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/KPi_nubes-groupfolder-files.svg?job=plot_general)

##### group-storage_[G].svg
![group-storage_[G].svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/KPi_nubes-group-storage_[G].svg?job=plot_general)

##### shares.svg
![shares.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/HZB-nubes/KPi_nubes-shares.svg?job=plot_general)

