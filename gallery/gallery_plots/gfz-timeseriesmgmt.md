# Timeseries

##### users_total.svg
![users_total.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-timeseriesmgmt/tsm-stats-production-users_total.svg?job=plot_general)

##### dashboards_total.svg
![dashboards_total.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-timeseriesmgmt/tsm-stats-production-dashboards_total.svg?job=plot_general)

##### panels_total.svg
![panels_total.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-timeseriesmgmt/tsm-stats-production-panels_total.svg?job=plot_general)

##### logins_last_7d.svg
![logins_last_7d.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-timeseriesmgmt/tsm-stats-production-logins_last_7d.svg?job=plot_general)

##### file_imports_last_7d.svg
![file_imports_last_7d.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-timeseriesmgmt/tsm-stats-production-file_imports_last_7d.svg?job=plot_general)

##### users_total.svg
![users_total.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-timeseriesmgmt/tsm-stats-staging-users_total.svg?job=plot_general)

##### dashboards_total.svg
![dashboards_total.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-timeseriesmgmt/tsm-stats-staging-dashboards_total.svg?job=plot_general)

##### panels_total.svg
![panels_total.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-timeseriesmgmt/tsm-stats-staging-panels_total.svg?job=plot_general)

##### logins_last_7d.svg
![logins_last_7d.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-timeseriesmgmt/tsm-stats-staging-logins_last_7d.svg?job=plot_general)

##### file_imports_last_7d.svg
![file_imports_last_7d.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/artifact/plots/gfz-timeseriesmgmt/tsm-stats-staging-file_imports_last_7d.svg?job=plot_general)

