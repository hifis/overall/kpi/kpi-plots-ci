# Overview of plots

- [awi-oceanandclimate](gallery_plots/awi-oceanandclimate.md)
- [webODV (AWI)](gallery_plots/awi-webodv.md)
- [desy-connect](gallery_plots/desy-connect.md)
- [HIFIS Events (DESY)](gallery_plots/desy-indico-events.md)
- [Jupyter (DESY)](gallery_plots/desy-jupyter.md)
- [Notes (DESY)](gallery_plots/desy-notes.md)
- [Rancher (DESY)](gallery_plots/desy-rancher.md)
- [InfiniteSpace (DESY)](gallery_plots/desy-storage-dcache.md)
- [SyncnShare (DESY)](gallery_plots/desy-syncnshare.md)
- [FileSender (DKFZ)](gallery_plots/dkfz-filesender.md)
- [LimeSurvey (DKFZ)](gallery_plots/dkfz-limesurvey.md)
- [B2Share (FZJ)](gallery_plots/FZJ-B2SHARE.md)
- [fzj-blablador](gallery_plots/fzj-blablador.md)
- [Helmholtz KG (FZJ)](gallery_plots/fzj-helmholtz-kg.md)
- [Juypter (FZJ)](gallery_plots/FZJ-Jupyter.md)
- [OpenStack (FZJ)](gallery_plots/FZJ-OpenStack.md)
- [gfz-earthquake-explorer](gallery_plots/gfz-earthquake-explorer.md)
- [GFZ-GIPP](gallery_plots/GFZ-GIPP.md)
- [GFZ-GraVIS](gallery_plots/GFZ-GraVIS.md)
- [gfz-icgem](gallery_plots/gfz-icgem.md)
- [RSD (GFZ)](gallery_plots/gfz-rsd.md)
- [Sensor Management System (GFZ)](gallery_plots/gfz-sensor-mgmt.md)
- [Timeseries](gallery_plots/gfz-timeseriesmgmt.md)
- [Helmholtz AAI](gallery_plots/helmholtz-aai.md)
- [helmholtz-backbone-vpn](gallery_plots/helmholtz-backbone-vpn.md)
- [helmholtz-cloud-services](gallery_plots/helmholtz-cloud-services.md)
- [LimeSurvey (HMGU)](gallery_plots/hmgu-limesurvey.md)
- [nubes (HZB)](gallery_plots/HZB-nubes.md)
- [SciFlow (HZB)](gallery_plots/hzb-sciflow.md)
- [Codebase (HZDR)](gallery_plots/hzdr-codebase.md)
- [Collabtex (HZDR)](gallery_plots/hzdr-collabtex.md)
- [GPU Compute (HZDR)](gallery_plots/hzdr-gpu-compute.md)
- [HIFIS Helpdesk (HZDR)](gallery_plots/hzdr-helpdesk.md)
- [Mattermost (HZDR)](gallery_plots/hzdr-mattermost.md)
- [bwSyncnShare (KIT)](gallery_plots/KIT-bwsns.md)
- [newsletter](gallery_plots/newsletter.md)
- [Sensor Management System (UFZ)](gallery_plots/ufz-sensor-mgmt.md)
- [workshops](gallery_plots/workshops.md)

# Overview of KPI plots

- [awi-oceanandclimate](gallery_plots_kpi/awi-oceanandclimate.md)
- [webODV (AWI)](gallery_plots_kpi/awi-webodv.md)
- [desy-connect](gallery_plots_kpi/desy-connect.md)
- [HIFIS Events (DESY)](gallery_plots_kpi/desy-indico-events.md)
- [Jupyter (DESY)](gallery_plots_kpi/desy-jupyter.md)
- [Notes (DESY)](gallery_plots_kpi/desy-notes.md)
- [Rancher (DESY)](gallery_plots_kpi/desy-rancher.md)
- [InfiniteSpace (DESY)](gallery_plots_kpi/desy-storage-dcache.md)
- [SyncnShare (DESY)](gallery_plots_kpi/desy-syncnshare.md)
- [FileSender (DKFZ)](gallery_plots_kpi/dkfz-filesender.md)
- [LimeSurvey (DKFZ)](gallery_plots_kpi/dkfz-limesurvey.md)
- [B2Share (FZJ)](gallery_plots_kpi/FZJ-B2SHARE.md)
- [fzj-blablador](gallery_plots_kpi/fzj-blablador.md)
- [Helmholtz KG (FZJ)](gallery_plots_kpi/fzj-helmholtz-kg.md)
- [Juypter (FZJ)](gallery_plots_kpi/FZJ-Jupyter.md)
- [OpenStack (FZJ)](gallery_plots_kpi/FZJ-OpenStack.md)
- [gfz-earthquake-explorer](gallery_plots_kpi/gfz-earthquake-explorer.md)
- [GFZ-GIPP](gallery_plots_kpi/GFZ-GIPP.md)
- [GFZ-GraVIS](gallery_plots_kpi/GFZ-GraVIS.md)
- [gfz-icgem](gallery_plots_kpi/gfz-icgem.md)
- [RSD (GFZ)](gallery_plots_kpi/gfz-rsd.md)
- [Sensor Management System (GFZ)](gallery_plots_kpi/gfz-sensor-mgmt.md)
- [Timeseries](gallery_plots_kpi/gfz-timeseriesmgmt.md)
- [Helmholtz AAI](gallery_plots_kpi/helmholtz-aai.md)
- [helmholtz-backbone-vpn](gallery_plots_kpi/helmholtz-backbone-vpn.md)
- [helmholtz-cloud-services](gallery_plots_kpi/helmholtz-cloud-services.md)
- [LimeSurvey (HMGU)](gallery_plots_kpi/hmgu-limesurvey.md)
- [nubes (HZB)](gallery_plots_kpi/HZB-nubes.md)
- [SciFlow (HZB)](gallery_plots_kpi/hzb-sciflow.md)
- [Codebase (HZDR)](gallery_plots_kpi/hzdr-codebase.md)
- [Collabtex (HZDR)](gallery_plots_kpi/hzdr-collabtex.md)
- [GPU Compute (HZDR)](gallery_plots_kpi/hzdr-gpu-compute.md)
- [HIFIS Helpdesk (HZDR)](gallery_plots_kpi/hzdr-helpdesk.md)
- [Mattermost (HZDR)](gallery_plots_kpi/hzdr-mattermost.md)
- [bwSyncnShare (KIT)](gallery_plots_kpi/KIT-bwsns.md)
- [newsletter](gallery_plots_kpi/newsletter.md)
- [Sensor Management System (UFZ)](gallery_plots_kpi/ufz-sensor-mgmt.md)
- [workshops](gallery_plots_kpi/workshops.md)
