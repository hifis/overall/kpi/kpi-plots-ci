"""This module checks for new services and adds them as submodules."""
import os
import gitlab
from git import Repo, Submodule

# use ci variable to get the project directory
repo = Repo(os.environ['CI_PROJECT_DIR'])

# get all projects from within the kpi group
gl = gitlab.Gitlab('https://codebase.helmholtz.cloud', oauth_token=os.environ['ACCESS_TOKEN_CI'])
group = gl.groups.get(5127)
projects = group.projects.list(get_all=True,order_by='name',sort="asc")

# iterate through the kpi projects and add them as submodule,
# except if they are the requirement or the calculation project
for group_project in projects:
    project = gl.projects.get(group_project.id)
    path = 'subprojects/' + project.path
    if project.name not in ('KPI Requirements', 'KPI Plots-Calculations-CI'):
        Submodule.add(repo, project.name, path, project.http_url_to_repo)
        print('Added ' + project.name + ' as a submodule.')
