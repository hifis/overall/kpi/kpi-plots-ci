"""Global variables are defined here to be used across the project."""
import os
from datetime import datetime
import pandas as pd
import pytz
from dateutil.parser import parse

# try to read environment variables
try:
    bot_access_token = os.environ['ACCESS_TOKEN_MM_BOT']
except KeyError:
    bot_access_token = ''

try:
    ci_pipeline_source = os.environ['CI_PIPELINE_SOURCE']
except KeyError:
    ci_pipeline_source = "unknown"

try:
    ci_project_url = os.environ['CI_PROJECT_URL']
except KeyError:
    ci_project_url = "https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci"

try:
    t_report = parse(os.environ['T_REPORT'])
except KeyError:
    t_report = datetime.now()

try:
    kpi_history = bool(os.environ['KPI_HISTORY'])
except KeyError:
    kpi_history = False

# set standard timezone for datetimes.
# this is being used for the comparation of timedates from different origins/formats
UTC = pytz.UTC

kpi_weights = pd.read_csv('./kpi-weights.csv')
