"""This module contains methods for special plots."""
import os
import re
from typing import List
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.patches as mpatches
import numpy as np
import pandas as pd
from dateutil.parser import parse
from datafile import Datafile
from column import Column
import config


def special_plot_type_1(datafile: Datafile, service_displayname: str):
    """Create and save a stackplot from the different columns of the datafile."""
    data_dict = {}
    for column in datafile.columns:
        # insert these values into a dict for the special plot.
        # the key is the column name and the value is the array containing the data
        data_dict[column.name] = column.values()

    # sort the dict by the average of the data values
    data_dict = dict(
        sorted(data_dict.items(), key=lambda x: sum(x[1]) / len(x[1])))

    labels_special_plot = list(data_dict.keys())

    # delete all labels, except the last 15 (these are the labels from the "biggest" plots)
    # this is not to overfill the legend
    for i, _ in enumerate(labels_special_plot):
        if i < len(labels_special_plot) - 15:
            labels_special_plot[i] = ""

    # print the stackplots, set legend, limits and title
    plt.clf()
    plt.stackplot(datafile.datetime_array(), data_dict.values(),
                  labels=labels_special_plot, alpha=0.7)
    plt.legend(loc="upper left", reverse=True)
    plt.xlim(left=parse('2020-01-01'))
    plt.ylim(bottom=0)
    plt.tick_params(axis='x', labelrotation=30)
    plt.xlabel('time')
    plt.title(service_displayname + ' - ' + datafile.filename_without_ending())

    # save the plot
    save_path = './artifact/plots/' + datafile.service_dir()
    if not os.path.exists(save_path):
        os.mkdir(save_path)
    plt.savefig(save_path + '/' + datafile.filename_without_ending() + '.pdf')
    plt.savefig(save_path + '/' + datafile.filename_without_ending() + '.svg')


def special_plot_type_2(datafile: Datafile):
    """
    Plot special plot type 2.

    Special plots type 2 is specifically for b2share_accounting.csv.
    Every row that holds a value for a specific Community gets dropped
    the only remaining rows are the overall values.
    """
    columns: List[Column] = []
    dataframe_only_overall = datafile.dataframe.drop(
        datafile.dataframe[(datafile.dataframe['Community'] != "overall")
                           & (datafile.dataframe['Community'] != "0")
                           & (datafile.dataframe['Community'] != "Name of Community")].index)
    for col_name in datafile.dataframe.columns:
        series = dataframe_only_overall[col_name]
        if len(series) > 2:
            # check if there is a #plot row and act accordingly.
            # if there is no #plot row, plot everything, except columns named comment
            if ((datafile.datetime_series[0] == '#plot' and series.iloc[0] == '1') or
                    datafile.datetime_series[0] != '#plot') and 'comment' not in col_name:
                # remove #unit and #plot rows, if they exist. save the unit as y_label
                if datafile.datetime_series[1] == '#unit':
                    y_label = series[1]
                    series.drop(series.index[1], inplace=True)
                else:
                    y_label = ''

                if datafile.datetime_series[0] == "#plot":
                    series.drop(series.index[0], inplace=True)

                # convert the datetime_array to an array of nums and delete all rows
                # that have been deleted from the series
                x_axis = mdates.date2num(datafile.datetime_array())
                indexes_to_be_deleted: List[int] = []
                for i in range(len(x_axis) - 1):
                    if i not in series.index:
                        indexes_to_be_deleted.append(i)

                x_axis = np.delete(x_axis, indexes_to_be_deleted)

                # set them as the index of the series
                series = series.set_axis(x_axis)

                # delete all NaN at the start and the end of the series
                series.replace('', np.nan, inplace=True)
                first_index = series.first_valid_index()
                last_index = series.last_valid_index()
                series = series.loc[first_index:last_index]

                # interpolate the data values to get rid of missing values
                series = pd.to_numeric(series, "coerce")
                series.interpolate(
                    inplace=True, limit_direction="both", method='index')

                columns.append(
                    Column(name=col_name, series=series, y_label=y_label))
    datafile.columns = columns


def special_plot_type_3(datafile: Datafile, service_displayname: str):
    """
    Plot special plot type 3.

    Special plot type 3 is specifically meant for helmholtz-aai - user_per_group.csv.
    it combines data from 2 different files to
    visualize non-helmholtz idp user and the overall value.
    """
    plt.clf()

    # copy the timedate column into an array
    time_date_array = datafile.datetime_series.array

    # remove #unit and #plot rows, if they exist.
    if datafile.datetime_series[1] == '#unit':
        time_date_array = np.delete(time_date_array, 1)  # type: ignore

    if datafile.datetime_series[0] == '#plot':
        time_date_array = np.delete(time_date_array, 0)  # type: ignore

    if re.search(r'\d\d\.\d\d\.\d\d', time_date_array[0]):
        x_axis = [parse(d, dayfirst=True) for d in time_date_array]
    else:
        x_axis = [parse(d) for d in time_date_array]

    data_userstats = pd.read_csv(
        "./subprojects/helmholtz-aai/stats/aai_userstats.csv", sep=',', keep_default_na=False)

    # save date column and drop it from dataframe
    time_date_series_userstats = data_userstats.iloc[:, 0]
    data_userstats.drop(
        columns=data_userstats.columns[0], axis=1, inplace=True)

    # calculate the overall user value by adding every value
    y_axis_userstats = []
    sum_value = 0
    for index in data_userstats.index:
        sum_value += data_userstats.at[index, 'all IdP'] * 0.98
        y_axis_userstats.append(sum_value)

    # convert time_date_series into array and parse it into datetime objects
    time_date_array_userstats = time_date_series_userstats.array
    x_axis_userstats = [parse(d, dayfirst=True)
                        for d in time_date_array_userstats]

    # plot the overall graph
    plt.plot(x_axis_userstats, y_axis_userstats,
             label='overall')  # type: ignore

    # use fill_between to simulate a stackplot for non-helmholtz idp users
    # using the combined data to fill the empty space
    y_axis_nhi = []
    x_axis_nhi = []
    for i, val in enumerate(x_axis_userstats):
        if (val.replace(tzinfo=config.UTC)
            >= datetime(year=2021, month=11, day=15, tzinfo=config.UTC)
                and val.replace(tzinfo=config.UTC) <= x_axis[-1].replace(tzinfo=config.UTC)):
            y_axis_nhi.append(y_axis_userstats[i])
            x_axis_nhi.append(x_axis_userstats[i])

    plt.fill_between(x_axis_nhi, y_axis_nhi, step="pre",
                     zorder=0, alpha=0.5, color='grey')  # type: ignore

    data_dict = {}
    for col_name in datafile.dataframe.columns:
        series = datafile.dataframe[col_name]
        series_without_headers = series.copy()

        # remove #unit and #plot rows, if they exist.
        if datafile.datetime_series[1] == '#unit':
            series_without_headers.drop(
                series_without_headers.index[1], inplace=True)

        if datafile.datetime_series[0] == '#plot':
            series_without_headers.drop(
                series_without_headers.index[0], inplace=True)

        # interpolate the data values to get rid of missing values
        series_without_headers.replace('', np.nan, inplace=True)
        series_without_headers = pd.to_numeric(
            series_without_headers, "coerce")
        series_without_headers.interpolate(
            inplace=True, limit_direction="forward", method="index")

        # then convert the values to arrays, so matplotlib can use them
        y_axis = series_without_headers.array.astype(float)
        # then insert these values into a dict for the special plot
        # the key is the column name and the value is the array containing the data
        data_dict[col_name] = y_axis

    # sort the dict by the average of the data values
    data_dict = dict(
        sorted(data_dict.items(), key=lambda x: sum(x[1]) / len(x[1])))

    labels_special_plot = list(data_dict.keys())

    # delete all labels, except the last 15 (these are the labels from the "biggest" plots)
    # this is not to overfill the legend
    for i, _ in enumerate(labels_special_plot):
        if i < len(labels_special_plot) - 15:
            labels_special_plot[i] = ""

    # print the stackplots, set legend, limits and title
    plt.stackplot(x_axis, data_dict.values(),
                  labels=labels_special_plot, alpha=0.7)
    handles, _ = plt.gca().get_legend_handles_labels()
    grey_patch = mpatches.Patch(color='grey', label='non helmholtz idp')
    handles.extend([grey_patch])
    plt.legend(loc="upper left", reverse=True, handles=handles)
    plt.xlim(left=parse('2020-01-01'))
    plt.ylim(bottom=0)
    plt.tick_params(axis='x', labelrotation=30)
    plt.xlabel('time')
    plt.title(service_displayname + ' - ' + datafile.filename_without_ending())

    # save the plot
    save_path = './artifact/plots/' + datafile.service_dir()
    if not os.path.exists(save_path):
        os.mkdir(save_path)
    plt.savefig(save_path + '/' + datafile.filename_without_ending() + '.pdf')
    plt.savefig(save_path + '/' + datafile.filename_without_ending() + '.svg')
