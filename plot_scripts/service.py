"""This module contains the Service class."""
import os
import csv
from typing import List
import pandas as pd
from datafile import Datafile


class Service:
    """This class represents a service with all its attributes and relevant functions."""

    def __init__(self, service_dir: str):
        """Initialize a service object."""
        self.service_dir = service_dir
        self.displayname = self.init_displayname()
        self.datafiles = self.init_datafiles()
        self.grace_period = self.init_grace_period()
        self.uid = self.init_uid()

    def init_uid(self) -> str:
        """Check if there is a uid defined in metadata and initialize it as attribute."""
        uid = ""
        uid_path = './metadata/' + self.service_dir + '/uid'
        if os.path.exists(uid_path):
            with open(uid_path, encoding='ISO 8859-1') as uid_file:
                uid = uid_file.read()
        return uid

    def init_displayname(self) -> str:
        """Initialize the displayname of the service as an attribute."""
        displayname_path = './metadata/' + self.service_dir + '/displayname'
        if os.path.exists(displayname_path):
            with open(displayname_path, encoding='ISO 8859-1') as displayname_file:
                displayname = displayname_file.read()
            return displayname.replace('\n', '')
        return self.service_dir

    def init_datafiles(self) -> list[Datafile]:
        """Initialize the datafile objects of the service as an attribute."""
        # get all datafile of the service
        datafiles: List[Datafile] = []
        datafile_paths: List[str] = []
        for result in os.walk('./subprojects/' + self.service_dir):
            for file in result[2]:
                filepath = result[0] + os.sep + file
                if (filepath.endswith(".csv") and
                    (filepath.split('/')[3] == 'stats' or filepath.split('/')[3] == 'data')
                        and os.path.exists(filepath)):
                    datafile_paths.append(filepath)
        datafile_paths.sort(key=lambda y: y.lower())
        for datafile_path in datafile_paths:
            # try opening the data file with pandas. if a ParserError occurs,
            # the format of the file is possibly wrong
            try:
                # check if csv is comma or semicolon separated,
                # and use it accordingly to open the csv as pandas dataframe
                with open(datafile_path, newline='', encoding='ISO 8859-1') as csvfile:
                    Dialect = csv.Sniffer().sniff(csvfile.read())
                dataframe = pd.read_csv(
                    datafile_path, sep=Dialect.delimiter, keep_default_na=False)
                datafiles.append(
                    Datafile(filepath=datafile_path, dataframe=dataframe))
            # if a ParserError occurs or the function times out,
            # the loop gets skipped and the error is logged to the console
            except (pd.errors.ParserError, csv.Error) as e:
                print('ERROR: pandas couldn`t open ' +
                      self.service_dir + ' - ' + datafile_path.split('/')[3])
                print('error message: ' + str(e))
        return datafiles

    def init_grace_period(self) -> int:
        """Check if there is a grace period defined in metadata and initialize it as attribute."""
        grace_period = 14
        grace_period_path = './metadata/' + self.service_dir + '/grace_period'
        if os.path.exists(grace_period_path):
            with open(grace_period_path, encoding='ISO 8859-1') as grace_period_file:
                grace_period = int(grace_period_file.read())
        return grace_period

    def kpi_weight_sum(self) -> float:
        """Get the rounded kpi weight sum of this service."""
        kpi_weight_sum = 0
        for datafile in self.datafiles:
            kpi_weight_sum += datafile.kpi_weight_sum()
        return round(kpi_weight_sum, 2)

    def kpi_sum(self) -> float:
        """Get the weighted service kpi."""
        service_kpi = 0
        for datafile in self.datafiles:
            for column in datafile.columns:
                service_kpi += column.kpi_raw * column.kpi_weight
        return service_kpi
