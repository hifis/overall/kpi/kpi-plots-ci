"""This module contains the Datafile class."""
import os
from datetime import datetime
import re
from typing import List
import matplotlib.dates as mdates
import pandas as pd
import numpy as np
from dateutil.parser import parse
from column import Column
import config


class Datafile:
    """This class represents a datafile with all its attributes and relevant functions."""

    def __init__(self, filepath: str, dataframe: pd.DataFrame):
        """Initialize a datafile object."""
        self.filepath = filepath
        self.dataframe = dataframe
        self.datetime_series = self.init_datetime_series()
        self.special_plot_type = self.init_special_plot_type()
        self.columns = self.init_columns()
        self.fitstart_overall = self.init_fitstart_overall()
        self.reference_overall = self.init_reference_overall()

    def init_datetime_series(self) -> pd.Series:
        """Extract the datetime column from the dataframe and sets it as attribute."""
        datetime_series = self.dataframe.iloc[:, 0]
        self.dataframe.drop(
            columns=self.dataframe.columns[0], axis=1, inplace=True)
        return datetime_series

    def init_special_plot_type(self) -> int:
        """
        Initialize special plot type.

        Check if there is a metadata file that specifies whether this file is a
        special plot and read the type of the special plot and initialize it as an attribute.
        """
        special_plot_path = './metadata/' + self.service_dir() + '/special_plot_' + \
            self.filename_without_ending()
        if os.path.exists(special_plot_path):
            with open(special_plot_path, encoding='ISO 8859-1') as special_plot_file:
                return int(special_plot_file.read())
        return 0

    def init_columns(self) -> list[Column]:
        """Initialize the column objects of the datafile as an attribute."""
        columns: List[Column] = []
        for col_name in self.dataframe.columns:
            series = self.dataframe[col_name]
            if len(series) > 2:
                # check if there is a #plot row and act accordingly.
                # if there is no #plot row, just plot everything, except column name is comment
                if ((self.datetime_series[0] == '#plot' and series[0] == '1') or
                        self.datetime_series[0] != '#plot') and 'comment' not in col_name:
                    # remove #unit and #plot rows, if they exist. save the unit as y_label
                    if self.datetime_series[1] == '#unit':
                        y_label = series[1]
                        series.drop(series.index[1], inplace=True)
                    else:
                        y_label = ''

                    if self.datetime_series[0] == "#plot":
                        series.drop(series.index[0], inplace=True)

                    # convert the datetime_array to an array of nums and
                    # set them as the index of the series
                    series = series.set_axis(
                        mdates.date2num(self.datetime_array()))

                    # delete all NaN at the start and the end of the series
                    series.replace('', np.nan, inplace=True)
                    first_index = series.first_valid_index()
                    last_index = series.last_valid_index()
                    series = series.loc[first_index:last_index]

                    # interpolate the data values to get rid of missing values
                    series = pd.to_numeric(series, "coerce")
                    series.interpolate(
                        inplace=True, limit_direction="both", method='index')

                    columns.append(
                        Column(name=col_name, series=series, y_label=y_label))
        return columns

    def init_fitstart_overall(self) -> datetime:
        """Initialize fitstart_overall timepoint of the datafile as an attribute."""
        # read logstart and onboard and set the later as fitstart_overall.
        # default onboard is 2021-03-29
        logstart = self.datetime_array()[0]

        onboard_path = './metadata/' + self.service_dir() + '/T_onboard'
        if os.path.exists(onboard_path):
            with open(onboard_path, encoding='ISO 8859-1') as onboard_file:
                onboard = parse(onboard_file.read().split('"')[1])
            # check if onboard lies ahead of t_report. if that is the case, dont use it
            if onboard > config.t_report:
                onboard = datetime(2021, 3, 29)
        else:
            onboard = datetime(2021, 3, 29)

        if onboard.replace(tzinfo=config.UTC) > logstart.replace(tzinfo=config.UTC):
            return onboard
        return logstart

    def init_reference_overall(self) -> datetime:
        """Initialize reference_overall timepoint of the datafile as an attribute."""
        # check if there is a predefined reference_overall, otherwise use T_fitstart_overall
        reference_overall_path = './metadata/' + \
            self.service_dir() + '/T_reference_overall'
        if os.path.exists(reference_overall_path):
            with open(reference_overall_path, encoding='ISO 8859-1') as reference_overall_file:
                reference_overall = parse(
                    reference_overall_file.read().split('"')[1])
            # check if parsed reference_overall is in the future. dont use it, if thats the case
            if reference_overall > config.t_report:
                reference_overall = self.fitstart_overall
        else:
            reference_overall = self.fitstart_overall
        return reference_overall

    def filename(self) -> str:
        """Split the filename from the filepath and returns it."""
        return self.filepath.split('/')[4]

    def filename_without_ending(self) -> str:
        """Split the filename without ending from the filepath and returns it."""
        return self.filepath.split('/')[4].split('.')[0]

    def service_dir(self) -> str:
        """Split the service_dir  from the filepath and returns it."""
        return self.filepath.split('/')[2]

    def datetime_array(self) -> list[datetime]:
        """Delete #unit and #plot row from datetime series and convert into array of datetime."""
        datetime_series_copy = self.datetime_series.copy()
        # check length of the datetime series, preventing an index out of range exeption.
        # If everyone kept to the format, this would not be necessary...
        if len(self.datetime_series) > 1:
            if self.datetime_series[1] == '#unit':
                datetime_series_copy = datetime_series_copy.drop(1)

        if self.datetime_series[0] == '#plot':
            datetime_series_copy = datetime_series_copy.drop(0)

        datetime_array = datetime_series_copy.array
        if re.search(r'\d\d\.\d\d\.\d\d', datetime_array[0]):
            return [parse(d, dayfirst=True) for d in datetime_array]
        return [parse(d) for d in datetime_array]

    def kpi_weight_sum(self) -> float:
        """Get the kpi weight sum of this datafile."""
        kpi_weight_sum = 0
        for column in self.columns:
            kpi_weight_sum += column.kpi_weight
        return kpi_weight_sum

    def latest_data_delta(self) -> int:
        """Get the time(in days) that passed since the last data value."""
        return (config.t_report.date() - self.datetime_array()[-1].date()).days
