"""This module contains the Column class."""
import pandas as pd


class Column:
    """This class represents a column with all its attributes and relevant functions."""

    def __init__(self, name: str, series: pd.Series, y_label: str):
        """Initialize a column object."""
        self.name = name
        self.series = series
        self.y_label = y_label
        self.kpi_raw: float = 0
        self.kpi_weight: float = 0
        self.is_user_data: bool = False

    def name_without_slash(self) -> str:
        """Replace slashes in col name with backslashes."""
        return self.name.replace('/', '\\')

    def name_without_slash_blank(self) -> str:
        """Replace slashes in col name with backslashes and blanks with underline."""
        return self.name.replace('/', '\\').replace(' ', '_')

    def values(self) -> list[float]:
        """Get the series as list of type float."""
        return self.series.array.astype(float).tolist()

    def index(self) -> list[float]:
        """Get the the datetimes of the series as list of type float."""
        return self.series.index.array.tolist()

    def set_kpi_raw(self, kpi_raw: float):
        """Set the kpi_raw value."""
        self.kpi_raw = kpi_raw

    def set_kpi_weight(self, kpi_weight: float):
        """Set the kpi_weight value."""
        self.kpi_weight = kpi_weight

    def set_is_user_data(self, is_user_data: bool):
        """Set the bool decididng whether this column is designated as user data."""
        self.is_user_data = is_user_data
