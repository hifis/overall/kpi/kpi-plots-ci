"""This module reads data from csv files, plots graphs and calculates KPI-values."""
import os
from typing import List
from datetime import datetime, timedelta
import json
import time
import math
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import pandas as pd
import mattermost
from dateutil.parser import parse
from pypdf import PdfMerger
from datafile import Datafile
from service import Service
import config
import special_plots


def main():
    """Control the plotting and calculation of kpi values."""
    start_time = time.time()
    # create/delete some directories and file
    if not os.path.exists('./artifact'):
        os.mkdir('./artifact')

    if not os.path.exists('./artifact/plots'):
        os.mkdir('./artifact/plots')

    if not os.path.exists('./artifact/kpi_plots'):
        os.mkdir('./artifact/kpi_plots')

    if not os.path.exists('./gallery'):
        os.mkdir('./gallery')

    if not os.path.exists('./gallery/gallery_plots'):
        os.mkdir('./gallery/gallery_plots')

    if not os.path.exists('./gallery/gallery_plots_kpi'):
        os.mkdir('./gallery/gallery_plots_kpi')

    if os.path.exists('./artifact/plots/plots.pdf'):
        os.remove('./artifact/plots/plots.pdf')

    if os.path.exists('./artifact/kpi_plots/kpi_plots.pdf'):
        os.remove('./artifact/kpi_plots/kpi_plots.pdf')

    # get all the paths of the subprojects
    services: List[Service] = []
    services_dict: dict[str, Service] = {}
    for directory in os.listdir('./subprojects/'):
        d = os.path.join('./subprojects/', directory)
        if os.path.isdir(d):
            service = Service(service_dir=directory)
            services.append(service)
            services_dict[directory] = service
    services.sort(key=lambda y: y.service_dir.lower())

    for service in services:
        for datafile in service.datafiles:
            if datafile.special_plot_type == 0:
                plot_and_calculate(datafile, service.displayname, True)
            elif datafile.special_plot_type == 1:
                special_plots.special_plot_type_1(
                    datafile, service.displayname)
            elif datafile.special_plot_type == 2:
                special_plots.special_plot_type_2(datafile)
                plot_and_calculate(datafile, service.displayname, True)
            elif datafile.special_plot_type == 3:
                special_plots.special_plot_type_3(
                    datafile, service.displayname)

    kpi_contribution: dict[str, float] = {}
    user_count_json: dict[str, float] = {}
    kpi_weight_array: List[dict[str, str]] = []
    gallery_overview_string = '# Overview of plots\n\n'
    gallery_overview_kpi_string = '\n# Overview of KPI plots\n\n'
    service_kpi_string = 'service_dir, kpi\n'
    sum_of_weights = 0
    total_kpi = 0
    kpi_list_string = 'displayname,service_slug,filename,col_name,kpi,weight,weighted kpi\n'

    for service in services:

        gallery_plots_string = "# " + service.displayname + "\n\n"
        gallery_plots_kpi_string = "# " + service.displayname + "\n\n"

        service_kpi_string += service.displayname + \
            ',' + str(service.kpi_sum()) + '\n'

        sum_of_weights += service.kpi_weight_sum()
        total_kpi += service.kpi_sum()

        if service.kpi_sum() != 0:
            kpi_contribution[service.displayname] = service.kpi_sum()

        # check if there is a service weight sum that is != 0 and != 1, which indicates an error
        # if that is the case, send a warning message to a mattermost channel
        if service.kpi_weight_sum() != 0 and service.kpi_weight_sum() != 1:
            message = service.displayname + ": KPI weight sum is " + str(service.kpi_weight_sum()) \
                + ". Could there be an error in kpi-weights.csv?"
            if config.ci_pipeline_source == "schedule":
                send_mm_message(message)

        for datafile in service.datafiles:
            # check, if the last time point of any data file is more than
            # 2 weeks(or the timeframe defined in metadata) away from T_Report.
            # if that is the case, send a warning message to a mattermost channel
            if datafile.kpi_weight_sum() != 0:
                if datafile.latest_data_delta() > service.grace_period:
                    message = service.displayname + ": Last data entry was " \
                        + str(datafile.latest_data_delta()) + " days ago for " \
                        + datafile.filename() + ". The defined grace period is "\
                        + str(service.grace_period) + " days."
                    if config.ci_pipeline_source == "schedule":
                        send_mm_message(message)

            for column in datafile.columns:
                # if the column is specified as is_user_data in kpi_weights.csv,
                # add the latest value to a dict for user_count.json
                if column.is_user_data is True and service.uid != "":
                    if datafile.latest_data_delta() <= service.grace_period:
                        user_count_json[service.uid] = {
                            "displayname": service.displayname,
                            "user_count": int(column.values()[-1])
                        }
                    else:
                        user_count_json[service.uid] = {
                            "displayname": service.displayname,
                            "user_count": 0
                        }

                # this is for the automated generation and checking of the kpi-weights.csv file.
                # every column, that is destined to be plotted, is added to this array
                if datafile.special_plot_type in (0, 2):
                    kpi_weight_array.append({
                        "service_slug": service.service_dir,
                        "filename": datafile.filename(),
                        "col_name": column.name
                    })

                    # create the md text for the gallery file integrating the svg
                    gallery_plots_string += (
                        f"##### {column.name_without_slash_blank()}"
                        f".svg\n![{column.name_without_slash_blank()}.svg]("
                        f"{config.ci_project_url}/-/jobs/artifacts/master/raw/artifact/plots/"
                        f"{service.service_dir}/{datafile.filename_without_ending()}-{column.name_without_slash_blank()}"
                        f".svg?job=plot_general)\n\n"
                    )

                    gallery_plots_kpi_string += (
                        f"##### {column.name_without_slash_blank()}"
                        f".svg\n![{column.name_without_slash_blank()}.svg]("
                        f"{config.ci_project_url}/-/jobs/artifacts/master/raw/artifact/kpi_plots/"
                        f"{service.service_dir}/{datafile.filename_without_ending()}-{column.name_without_slash_blank()}"
                        f".svg?job=plot_general)\n\n"
                    )

                    # generation of string for the kpi list containing every single kpi value
                    kpi_list_string += f"{service.displayname},{service.service_dir},{datafile.filename()},{column.name},{column.kpi_raw},{column.kpi_weight},{str(round(column.kpi_raw * column.kpi_weight, 4))}\n"

            # create gallery string for special plots
            if datafile.special_plot_type in (1, 2):
                # create the md text for the gallery file integrating the svg
                gallery_plots_string += "##### " + datafile.filename_without_ending() + ".svg\n![" \
                    + datafile.filename_without_ending() + ".svg](" \
                    + config.ci_project_url + "/-/jobs/artifacts/master/raw/artifact/plots/" \
                    + datafile.service_dir() + "/" + datafile.filename_without_ending() \
                    + ".svg?job=plot_general)\n\n"

        # print the gallery files for the individual services with and without kpi
        gallery_plots_path = f"./gallery/gallery_plots/{service.service_dir}.md"
        with open(gallery_plots_path, "w", encoding='utf-8') as file:
            file.write(gallery_plots_string)

        gallery_plots_kpi_path = f"./gallery/gallery_plots_kpi/{service.service_dir}.md"
        with open(gallery_plots_kpi_path, "w", encoding='utf-8') as file:
            file.write(gallery_plots_kpi_string)

        # create the string for the gallery overview md file
        gallery_overview_string += "- [" + service.displayname + "](gallery_plots/" \
            + service.service_dir + ".md)\n"
        gallery_overview_kpi_string += "- [" + service.displayname + "](gallery_plots_kpi/" \
            + service.service_dir + ".md)\n"

    # print the gallery overview file for all services
    with open("./gallery/gallery.md", "w", encoding='utf-8') as text_file:
        text_file.write(gallery_overview_string + gallery_overview_kpi_string)

    # print the service kpi values to a csv file
    with open("./artifact/service_kpis.csv", "w", encoding='ISO 8859-1') as text_file:
        text_file.write(service_kpi_string)

    # print the kpi list to a csv file
    with open("./artifact/kpi_list.csv", "w", encoding='utf-8') as text_file:
        text_file.write(kpi_list_string)

    # print the total kpi alongside some other info to a csv file
    total_kpi_string = '#T_report,Sum of weights(should equal number of services),Total KPI,' \
        + 'KPI per service\n' + str(config.t_report.date()) + ',' + str(sum_of_weights) + ',' \
        + str(round(total_kpi, 4)) + ',' + \
        str(round(total_kpi / sum_of_weights, 4))
    with open("./artifact/total_kpi.csv", "w", encoding='ISO 8859-1') as text_file:
        text_file.write(total_kpi_string)

    # plot and save the kpi contribution per service graph
    plt.clf()
    plt.bar(list(kpi_contribution.keys()), list(kpi_contribution.values()), color="#002864")
    plt.tick_params(axis='x', labelrotation=90, labelsize=6)
    plt.ylabel("KPI contribution per service")
    plt.title("Contribution of services to overall KPI")
    plt.tight_layout()

    save_path = './artifact'
    plt.savefig(save_path + '/kpi_contributions_per_service.pdf')
    plt.savefig(save_path + '/kpi_contributions_per_service.svg')

    # check whether there are rows in kpi_weights.csv for which no corresponding data column exists
    # and flag them via the notes column
    for index in config.kpi_weights.index:
        if not ({
            "service_slug": config.kpi_weights.at[index, "service_slug"],
            "filename": config.kpi_weights.at[index, "file_name"],
            "col_name": config.kpi_weights.at[index, "col_name"]
        }) in kpi_weight_array:
            if float(config.kpi_weights.at[index, "weight"]) == 0:
                config.kpi_weights.drop(index, inplace=True)
            else:
                config.kpi_weights.at[index, "notes"] = "this data column might no longer exist"
        else:
            config.kpi_weights.at[index, "notes"] = ""
            config.kpi_weights.at[index, "source"] = (
                'https://codebase.helmholtz.cloud/hifis/overall/kpi/'
                f'{config.kpi_weights.at[index, "service_slug"]}')
            for weight in kpi_weight_array:
                if weight['service_slug'] == config.kpi_weights.at[index, "service_slug"]:
                    config.kpi_weights.at[index, "displayname"] = services_dict[weight['service_slug']].displayname

    # check if there are data columns that have no kpi weight in kpi-weights.csv. if thats the case,
    # insert them into kpi-weights.csv
    for weight in kpi_weight_array:
        if not (((config.kpi_weights['service_slug'] == weight['service_slug'])
                & (config.kpi_weights['file_name'] == weight['filename'])
                & (config.kpi_weights['col_name'] == weight['col_name'])).any()):
            new_row = {
                'displayname': services_dict[weight['service_slug']].displayname,
                'service_slug': weight['service_slug'],
                'file_name': weight['filename'],
                'col_name': weight['col_name'],
                'weight': 0,
                'user_count_row': 0,
                "source": f"https://codebase.helmholtz.cloud/hifis/overall/kpi/{weight['service_slug']}"
            }
            # type: ignore
            config.kpi_weights.loc[len(config.kpi_weights)] = new_row
            print(f"added {weight['service_slug']} - {weight['filename']} - {weight['col_name']} to kpi-weights.csv")

    config.kpi_weights.sort_values(
        ['displayname', 'service_slug', 'file_name', 'col_name'],
        inplace=True,
        key=lambda col: col.str.lower()
    )
    config.kpi_weights.to_csv('./kpi-weights.csv', index=False)

    # create json file with the latest user count of each service
    with open('./artifact/user_count.json', 'w', encoding='ISO 8859-1') as json_file:
        json.dump(user_count_json, json_file, indent=4)

    # create and save a merged pdf for both the normal and the kpi plots
    merger_plots = PdfMerger()
    pdf_plotfiles = []
    for subdir, _, files in os.walk('./artifact/plots/'):
        for file in files:
            filepath = subdir + os.sep + file
            if filepath.endswith(".pdf"):
                pdf_plotfiles.append(filepath)

    for filepath in pdf_plotfiles:
        merger_plots.append(filepath)

    merger_plots.write('./artifact/plots/plots.pdf')
    merger_plots.close()

    merger_kpi_plots = PdfMerger()
    pdf_kpi_plotfiles = []
    for subdir, _, files in os.walk('./artifact/kpi_plots/'):
        for file in files:
            filepath = subdir + os.sep + file
            if filepath.endswith(".pdf"):
                pdf_kpi_plotfiles.append(filepath)

    for filepath in pdf_kpi_plotfiles:
        merger_kpi_plots.append(filepath)

    merger_kpi_plots.write('./artifact/kpi_plots/kpi_plots.pdf')
    merger_kpi_plots.close()

    # add kpi value to kpi_history.csv if pipeline is scheduled
    # then print kpi history plots
    if config.ci_pipeline_source == 'schedule':

        kpi_history_row = (f"\n{config.t_report.date()},{sum_of_weights},"
                           f"{round(total_kpi, 4)},{round(total_kpi / sum_of_weights, 4)}")

        with open('./kpi_history.csv', 'a', encoding='utf-8') as file:
            file.write(kpi_history_row)

    kpi_history = pd.read_csv('./kpi_history.csv')

    history_dates = kpi_history['T_Report'].array
    history_dates = [parse(d) for d in history_dates]

    history_total_kpi = kpi_history['Total KPI'].array.astype(
        float).tolist()
    history_kpi_per_service = kpi_history['KPI per service'].array.astype(
        float).tolist()

    # plot graph showing the development of the total kpi
    plt.clf()
    plt.plot(history_dates, history_total_kpi)
    plt.xlim(left=history_dates[0], right=history_dates[-1])
    plt.ylim(bottom=0)
    plt.tick_params(axis='x', labelrotation=30)
    plt.xlabel('time')
    plt.ylabel("KPI")
    plt.title("KPI History")

    # save the plot as pdf and svg
    save_path = './artifact/kpi_history_plot'
    plt.savefig(save_path + '.pdf')
    plt.savefig(save_path + '.svg')

    # plot graph showing the development of the kpi per service
    plt.clf()
    plt.plot(history_dates, history_kpi_per_service)
    plt.xlim(left=history_dates[0], right=history_dates[-1])
    plt.ylim(bottom=0)
    plt.tick_params(axis='x', labelrotation=30)
    plt.xlabel('time')
    plt.ylabel("KPI per service")
    plt.title("KPI History")

    # save the plot as pdf and svg
    save_path = './artifact/kpi_per_service_history_plot'
    plt.savefig(save_path + '.pdf')
    plt.savefig(save_path + '.svg')

    print("Runtime: " + str(timedelta(seconds=time.time() - start_time)))


def send_mm_message(message_string: str):
    """
    Send a mattermost message.

    This function uses a bot to send a message to a mattermost channel.
    it utilizes the python mattermost library for easier access to the mattermost api.
    """
    if config.bot_access_token == '':
        return
    mm = mattermost.MMApi("https://mattermost.hzdr.de/api")
    mm.login(bearer=config.bot_access_token)
    mm.create_post('oq7z3wpmqfgctnkwxcokwwzadh', message_string)
    mm.revoke_user_session()


def plot_and_calculate(datafile: Datafile, service_displayname: str, plot: bool):
    """Calculate kpi values and optionally plot both normal and kpi graphs."""
    for column in datafile.columns:
        print(datafile.service_dir() + " - " +
              datafile.filename() + " - " + column.name)
        x_axis = column.index()
        y_axis = column.values()

        # get the index that is closest to T_report
        closest_date_index_t_report = x_axis.index(min(x_axis, key=lambda d: abs(
            d - mdates.date2num(config.t_report))))  # type: ignore

        # only use data that is from before t_report
        x_axis = x_axis[0:closest_date_index_t_report + 1]
        y_axis = y_axis[0:closest_date_index_t_report + 1]

        # if there is no data left(eg no data available before t_report), return
        if len(x_axis) == 0:
            column.set_kpi_raw(0)
            column.set_kpi_weight(0)
            column.set_is_user_data(False)
            print("No data left, aborting plotting")
            return

        plt.clf()
        _, ax = plt.subplots()

        if plot is True:
            # plot the data, set axis labels and title
            plt.plot(x_axis, y_axis)
            plt.xlim(left=datetime(2020, 1, 1),
                     right=config.t_report + timedelta(days=30))
            plt.ylim(bottom=0)
            plt.tick_params(axis='x', labelrotation=30)
            plt.xlabel('time')
            plt.ylabel(column.y_label)
            plt.title(service_displayname + ' - ' + datafile.filename() + ' - ' + column.name)

            # save the plot as pdf and svg
            save_path = './artifact/plots/' + datafile.service_dir()
            if not os.path.exists(save_path):
                os.mkdir(save_path)
            plt.savefig(save_path + '/' + datafile.filename_without_ending() + '-' +
                        column.name_without_slash_blank() + '.pdf')
            plt.savefig(save_path + '/' + datafile.filename_without_ending() + '-' +
                        column.name_without_slash_blank() + '.svg')

        # get the data value, that is closest to T_fitstart_overall
        closest_date_index_reference = x_axis.index(min(x_axis, key=lambda d: abs(
            d - mdates.date2num(datafile.fitstart_overall))))  # type: ignore

        x_axis_timeframe = x_axis[closest_date_index_reference:len(x_axis)]
        y_axis_timeframe = y_axis[closest_date_index_reference:len(y_axis)]

        # interpolate to T_report if necessary
        if x_axis_timeframe[len(x_axis_timeframe) - 1] < mdates.date2num(config.t_report):
            y_axis_timeframe.append(np.interp(mdates.date2num(
                config.t_report), x_axis_timeframe, y_axis_timeframe))  # type: ignore
            x_axis_timeframe.append(mdates.date2num(
                config.t_report))  # type: ignore

        # then use polyfit and use the results to plot the raw fitting line
        m, b = np.polyfit(x_axis_timeframe, y_axis_timeframe, 1)
        if plot is True:
            plt.plot(
                x_axis_timeframe,
                [m * x + b for x in x_axis_timeframe],
                linestyle='dashed',
                color='grey',
                linewidth=0.5
            )

        # get the data value, that is closest to T_reference_overall.
        # then check if the value is earlier than t_reference_overall.
        # if that is the case, increase the index by one, using the next value
        x_closest_date_reference_index = x_axis.index(min(x_axis, key=lambda d: abs(
            d - mdates.date2num(datafile.reference_overall))))  # type: ignore

        closest_date_reference = x_axis[x_closest_date_reference_index]

        if closest_date_reference < mdates.date2num(datafile.reference_overall):
            x_closest_date_reference_index += 1

        # define the timeframe, for the reference
        y_axis_reference_timeframe = y_axis[x_closest_date_reference_index:len(
            y_axis)]

        # if there is no data left(eg reference_overall later than t_report), return
        if len(y_axis_reference_timeframe) == 0:
            column.set_kpi_raw(0)
            column.set_kpi_weight(0)
            column.set_is_user_data(False)
            print("No data left, aborting plotting")
            return

        # calculate the kpi value
        kpi_e = m * mdates.date2num(config.t_report) + b
        kpi_a = m * mdates.date2num(datafile.reference_overall) + b

        minimum = min(y_axis_reference_timeframe)
        maximum = max(y_axis_reference_timeframe)

        kpi_a = max(kpi_a, minimum)
        kpi_e = min(kpi_e, maximum)

        if plot is True:
            # print the effective fitting line
            plt.plot([x_axis_timeframe[0], x_axis_timeframe[-1]], [kpi_a,
                     kpi_e], linestyle='dashed', color='green', linewidth=2)
            plt.scatter([x_axis_timeframe[0]], [kpi_a],
                        marker='o', color='black')
            plt.scatter([x_axis_timeframe[-1]], [kpi_e],
                        marker='v', color='red')

        kpi_raw = 0
        if kpi_a > 0:
            kpi_raw = kpi_e / kpi_a - 1

        kpi_raw = round(kpi_raw, 2)

        # concatenate the string used for the legend
        legend = 'm=' + f"{m:.1e}" + ', y0=' + f"{b:.1e}" + '\n' \
            + 'T_reference_overall=' + str(datafile.reference_overall.date()) \
            + ', T_report=' + str(config.t_report.date()) + '\n'

        # check if we actually want to consider this value
        # (service was on board long enough, i.e. >=90 days)
        if (x_axis[0] + 90) <= mdates.date2num(config.t_report):
            legend += 'kpi_raw=' + str(kpi_raw) + ' - subject to weighting.'
        else:
            legend += 'kpi_raw=' + \
                str(kpi_raw) + ' - NOT CONSIDERED: less than 90 days data available.'
            kpi_raw = 0

        if plot is True:
            # print the legend in the upper left corner and set the y limits of the graph
            max_y_value = max(
                [maximum, m * x_axis_timeframe[len(x_axis_timeframe) - 1] + b])
            if math.isnan(max_y_value):
                print("WARNING: found NaN, plotting of this graph gets skipped")
            else:
                plt.text(0.03, 0.97, legend,
                        horizontalalignment='left',
                        verticalalignment='top',
                        transform=ax.transAxes)
                plt.ylim(bottom=0, top=max_y_value * 1.05 + 1)

                # save the kpi_plot as pdf
                save_path = './artifact/kpi_plots/' + datafile.service_dir()
                if not os.path.exists(save_path):
                    os.mkdir(save_path)
                plt.savefig(save_path + '/' + datafile.filename_without_ending() + '-' +
                            column.name_without_slash_blank() + '.pdf')
                plt.savefig(save_path + '/' + datafile.filename_without_ending() + '-' +
                            column.name_without_slash_blank() + '.svg')
        plt.close()

        # check if there is a weight for the plot in kpi-weights.csv.
        # if not, print a warning and use 0 as weight
        query_string = "service_slug=='" + datafile.service_dir() + "' and file_name=='" \
            + datafile.filename() + "' and col_name=='" + column.name + "'"
        kpi_weight_row = config.kpi_weights.query(query_string)
        if not kpi_weight_row.empty:
            kpi_weight = kpi_weight_row.iloc[0]['weight']
            is_user_data = bool(kpi_weight_row.iloc[0]['user_count_row'])
        else:
            kpi_weight = 0
            is_user_data = False

        column.set_kpi_raw(kpi_raw)
        column.set_kpi_weight(kpi_weight)
        column.set_is_user_data(is_user_data)


if __name__ == "__main__":
    main()
